# Hacker News Suite

An open source, unofficial Chrome extension to make Hacker News a little nicer

## Getting Started

### Installing

To install Hacker News Suite, visit [the Hacker News Suite homepage](https://hnsuite.com) and click `Install`.

### Development

To get started on developing Hacker News Suite, you may either [fork](https://gitlab.com/brendan/hnsuite/forks/new) this repository or [request direct access](https://gitlab.com/brendan/hnsuite/project_members/request_access) to this project.

Once you have access or your fork:

* Clone the repository locally
* Create an [issue](https://gitlab.com/brendan/hnsuite/issues/new) or use an [exisiting issue](https://gitlab.com/brendan/hnsuite/issues) for your feature or bug fix
* Create a feature branch based on that issue
* To load your version of the extension, follow these steps:
  - Navigate to [chrome://extensions](chrome://extensions)
  - Click `Load unpacked`
  - Navigate to the root of this repository
  - Click `Select`
* Make your changes and commit them to your branch
* [Submit a Merge Request](https://gitlab.com/brendan/hnsuite/merge_requests/new) to master an assign it to [@brendan](https://gitlab.com/brendan)

## Versioning

The SSOT for the version is in the [mainfest.json](https://gitlab.com/brendan/hnsuite/blob/master/manifest.json#L3).  For each release, we create a [tag](https://gitlab.com/brendan/hnsuite/tags) in the repository which kicks off the required CI/CD.

## Authors

* [**Brendan O'Leary**](https://gitlab.com/brendan) - Original author.  [Contact](https://twitter.com/olearycrew)

[See all contributors](https://gitlab.com/brendan/hnsuite/graphs/master)

## License 

This project is licensed under the MIT License - see the [LICENSE](https://gitlab.com/brendan/hnsuite/blob/master/LICENSE) file for details

## Acknowledgments

* Inspired by the fully-featured amazing [Reddit Enhancement Suite](https://redditenhancementsuite.com/).  Hacker News Suite wants to be RES when it grows up
* Shout out to [Sid](https://gitlab.com/sytses) for navigating Hacker News without this, but I needed help 😉