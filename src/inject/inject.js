chrome.extension.sendMessage({}, function(response) {
	var readyStateCheckInterval = setInterval(function() {
	if (document.readyState === "complete") {
		clearInterval(readyStateCheckInterval);
		checkDarkMode();

		// ----------------------------------------------------------
		// This part of the script triggers when page is done loading
		// ----------------------------------------------------------
		
		window.hnextDev = false

		logit($('.athing'))
		window.currentRowArrayKey = -1

		logit(window.currentRowArrayKey)

		// IF there's a selected row, select that row
		if(window.location.hash) {
			logit(window.location.hash);
			selectRowById(window.location.hash.replace("#", ""), $('.athing'))
		} 

		window.addEventListener('keypress', handleKeyPress, true);

		chrome.storage.onChanged.addListener(function(changes, namespace) {
			checkDarkMode();
		});

	}
	}, 10);
});


function handleKeyPress(e) {
	logit(e)
	let things = $('.athing');

	switch(e.key) {
		case 'j':
			e.preventDefault();
			incrementRow();
			selectRow(things);
		  //code block
		  break;
		case 'k':
			e.preventDefault();	
			decrementRow()
			selectRow(things);
		  //code block
		  break;
		case 'c':
		e.preventDefault();
			openComments(things);
			break;
		case 'h':
		e.preventDefault();
			hideIt(things, e);
			break;
		case 'u':
			//e.preventDefault();
			//openComments(things, 'user');
			break;
		case 'Enter': 
		e.preventDefault();
			openLink(things);
			break;
		case ' ':
		e.preventDefault();
			openLink(things);
			break;
		case 'v':
			e.preventDefault();
			vote(things);
			break;
		case 'r':
			e.preventDefault();
			reply(things);
			break;
		case '-':
			e.preventDefault();
			collapse(things);
			break;
		case '=':
			e.preventDefault();
			expand(things);
			break;
		default:
		  //code block
	  }
}

function openComments(things) {
	let currRow = $('#' + things[window.currentRowArrayKey].id)
	let links = getCommentLinks(currRow);
	logit(links);
	let url = links.comments.href;
	logit(url);
	openInNewTab(url)
}

function hideIt(things) {
	let currRow = $('#' + things[window.currentRowArrayKey].id)
	let hideButton = currRow.next('tr').children().eq(1).children().eq(4) 
	logit(hideButton[0])
	window.location.href = hideButton[0].href;
}

function openLink(things) {
	let currRow = $('#' + things[window.currentRowArrayKey].id)
	logit(currRow.children().eq(2).children().eq(0)[0].href);
	openInNewTab(currRow.children().eq(2).children().eq(0)[0].href);
}

function logit(obj) {
	if (window.hnextDev) {
		console.log(obj);
	}
}

function openInNewTab(url) {
	var win = window.open(url, '_blank');
	win.focus();
  }

function isScrolledIntoView(elem)
{
    var docViewTop = $(window).scrollTop();
    var docViewBottom = docViewTop + window.innerHeight;

    var elemTop = $(elem).offset().top;
	var elemBottom = elemTop + $(elem).height();
	
	//logit({ docViewTop: docViewTop, docViewBottom: docViewBottom, elemTop: elemTop, elemBottom: elemBottom} )

    return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
}

function scrollTo(elem) {
	$('html, body').stop()
	$('html, body').animate({
		scrollTop: (elem.first().offset().top)
	},500);
}