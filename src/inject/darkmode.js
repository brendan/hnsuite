function checkDarkMode() {
    let defOpts = {
        chkDarkMode: false
    }

    chrome.storage.sync.get(defOpts, function(items) {
        if (items.chkDarkMode) {
            setDarkMode();
        } else {
            unsetDarkMode();
        }
    });    
}

function setDarkMode() {
    $('body').addClass('darkBG', 10);
    setHeaderBG();
    $('#hnmain').addClass('midBG', 10);
    $('a').addClass('lightOrange', 10);
    $('.c00').addClass('lightText');
}

function unsetDarkMode() {
    $('.darkBG').removeClass('darkBG', 10);
    $('#hnmain').removeClass('midBG', 10);
    $('a').removeClass('lightOrange', 10);
    $('.c00').removeClass('lightText');
}

function setHeaderBG() {
    let headerHTML = $('#hnmain tbody tr').first()[0].innerHTML;
    headerHTML = headerHTML.replace("#ff6600", "#a84300")
    $('#hnmain tbody tr').first()[0].innerHTML = headerHTML;
}