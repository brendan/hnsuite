function incrementRow() {
	window.currentRowArrayKey++;
}

function decrementRow() {
	window.currentRowArrayKey--;
}

$(document).ready(function(){
    $(".athing").click(
        function(e){
            selectRowById(e.currentTarget.id, $(".athing"))
        }
      )
});

function selectRow(things) {
	logit(window.currentRowArrayKey)
	logit(things.length);
	if (window.currentRowArrayKey == things.length) {
		let moreLink = $('.morelink')
		window.location.href = moreLink[0].href;
		
	} else {
		if (window.currentRowArrayKey > -1 && window.currentRowArrayKey < things.length) {
			selectRowById(things[window.currentRowArrayKey].id, things);
		} 
		else {
			if (window.currentRowArrayKey > things.length) { window.currentRowArrayKey = things.length } else {
				window.currentRowArrayKey = -1
			}
		}
	}
}

function selectRowById(id, things) {
	let currRow = $('#' + id)
	$('tr').removeClass('hnextSelectedRow');
	currRow.addClass('hnextSelectedRow');

	if (!(currRow.hasClass('comtr'))) {
		// If on the home page, select the comment row.  Otherwise (on the comment page), don't
		currRow.next('tr').addClass('hnextSelectedRow')
    }
    
    setArrayIndex(id, things);

	if (!(isScrolledIntoView(currRow))) { scrollTo(currRow) }
}

function setArrayIndex(id, things) {
    $.each(things, function(i, e) {
        if (e.id === id) {
            window.currentRowArrayKey = i
        }
    });
}
