function reply(things) {
	let replyLink = $('#' + things[window.currentRowArrayKey].id).find('.reply').find('a')
	window.location.href = replyLink[0].href;
	logit(currRow);
}

function vote(things) {
	let vote = $('#' + things[window.currentRowArrayKey].id).find('.votelinks').find('a')
	logit(vote)
	window.location.href = vote[0].href;
	
}

function getCommentLinks(currRow) {
	let elems = currRow.next('tr').children().eq(1).children();
	return {
		user: elems[1],
		hide: elems[4],
		comments: elems[5]
	}
}