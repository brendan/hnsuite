$(document).ready(function(){
    $('body').on('click', 'a', function(){
      chrome.tabs.create({url: $(this).attr('href')});
      return false;
    });

    let defOpts = {
      chkDarkMode: false
    }

    chrome.storage.sync.get(defOpts, function(items) {
      $('#chkDarkMode').prop('checked', items.chkDarkMode)
    });

    $(".checkbox").change(function() {
      let opts = defOpts;

      if(this.checked) {
        opts[this.id] = true;
      } else {
        opts[this.id] = false;
      }

      chrome.storage.sync.set(opts, function() {
        var status = $('#optsSaved');
        status.fadeIn("slow");
        setTimeout(function() {
          status.fadeOut("slow");
        }, 1500);
      });

  });
 });